// IIFE
(function() {
  // shorthand for $(document).ready
  $(function() {
    ! function($) {
      $(function() {
        //$('#home, #about, #footer').carousel({});
        var $root = $('html, body');
        $('a').click(function() {
          var href = $.attr(this, 'href');
          $root.animate({
            scrollTop: $(href).offset().top
          }, 500, function() {
            window.location.hash = href;
          });
          if ( $('button.navbar-toggler').attr('aria-expanded') == 'true') {
            $('.navbar-toggler').click();
          }
          return false;
        });
      });
    }(window.jQuery);
    // validate form
    $('#submit').attr('disabled', true);
    $('#submit').removeClass('btn-primary').addClass('btn-danger');
    $('.validate').bind("change paste keyup", function() {
      var count = 0;
      $.each($('.validate'), function() {
        if ($(this).val() !== '') {
          count++;
        }
      });
      if (count == 4) {
        $('#submit').attr('disabled', false);
        $('#submit').removeClass('btn-danger').addClass('btn-primary');
      } else {
        $('#submit').attr('disabled', true);
        $('#submit').removeClass('btn-primary').addClass('btn-danger');
      }
    });
  });
}());
